import { useState, useEffect } from 'react'

const ItemListContainer = ({ greeting }) => {
    const [ count , setCount ] = useState(3) // => [0, f()] / no if no for no while
    const [ date , setDate ] = useState(Date()) // => [0, f()] / no if no for no while
    const [boolean, setBoolean ] = useState(true) // => [0, f()] / no if no for no while
    
    const aumnentar = () => {   
        // count++ // count = count + 1 count += 1
        setCount(count + 3) // 1, 2 
        setDate(Date())  
    }

    const cambiarEstado = () => {
        setBoolean(!boolean)
    }
    // addEventListener('click', funcion )
    useEffect(()=>{
        console.log('addEventlistener')// varios segundos
        return () => {
        console.log('remover enentListener')// varios segundos
        }
    })

    // useEffect(()=>{
    //   console.log('Llamada a la api 2')// Una sola vez en el montado
    //   // set() lo que traigo de la api
    // }, [])

    // useEffect(()=>{
    //   console.log('CAmbio de estado 3')// Una sola vez en el montado
    //   // set() lo que traigo de la api
    // }, [boolean, count])

    
  
  console.log('ItemListContainer  4')
  

    return (
        <div>
            { date }
            {count}
            <button onClick={aumnentar}>Aumentar</button>
            <button onClick={cambiarEstado}>CAmbiar</button>

        </div>
    )
}

export default ItemListContainer



{/* <div            
    className='col-md-4 p-1'                                                           
>                    
    <div className="card w-100 mt-5" >
        <div className="card-header">
            {`${prod.name} - ${prod.categoria}`}
        </div>
        <div className="card-body">
            <img src={prod.foto} alt='' className='w-50' />
            {prod.stock}                                                            
        </div>
        <div className="card-footer"> 
            <Link to={`/detalle/${prod.id}`} >
                <button className="btn btn-outline-primary btn-block">
                    detalle del producto
                </button>   
            </Link> 
        </div>
    </div>                                                                                                                            
</div> */}