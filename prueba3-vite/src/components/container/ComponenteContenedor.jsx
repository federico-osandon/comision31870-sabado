import Formulario from "../Formulario/Formulario"
import Titulo from "../Titulo/Titulo"


const ComponenteContenedor = () => {
    let titulo = 'Titulo de App' // estado de App
    let subTitulo = 'Subtitulo de App' // estado de App
    //llamadas apis
  return (
    <div>
        <Titulo titulo={ titulo } subTitulo={ subTitulo } />           
        <Formulario />
        
        
    </div>
  )
}

export default ComponenteContenedor