import { memo } from "react"
import { Link } from "react-router-dom"

const Item = memo(
    ({ prod } ) => {
        console.log('item')
      return (
            <div 
                key={prod.id}            
                className='col-md-4 p-1'                                                           
            >                    
            <Link to={`/detail/${prod.id}`} style={{textDecoration: 'none'}}>
                <div className="card w-100 mt-5"  >
                    <div className="card-header">
                        
                        {`${prod.name} - ${prod.categoria}`}
                    </div>
                    <div className="card-body">
                        <img src={prod.foto} alt='' className='w-50' />
                        {prod.stock}                                                            
                    </div>
                    <div className="card-footer"> 
                            <button className="btn btn-outline-primary btn-block">
                                detalle del producto
                            </button>                                               
                    </div>
                </div>                                                                                                                            
            </Link>                                              
            </div>
      )
    }

)

export default Item