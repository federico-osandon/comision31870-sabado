import { memo } from 'react'
import { Link } from "react-router-dom"
import Item from "../Item/Item"

// memo(componente) -> memo(componente, función de comparación)
const ItemList = memo(
        ({products}) => {
            console.log('ItemList')
            return (
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                    }}
                >
                    {    
                        products.map(prod =>  <Item key={prod.id} prod={prod} />) 
                    }
                    
                </div>
            )
        }

)

export default ItemList