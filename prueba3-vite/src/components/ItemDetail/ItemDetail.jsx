import { useCartContext } from "../../context/CartContext"
import ItemCount from "../ItemCount/ItemCount"

const ItemDetail = ({product}) => {

    const { addProduct, cart } = useCartContext()
    
    const onAdd = (count) => {
        console.log(count)
        addProduct( { cantidad: count, ...product })
    }

    console.log(cart)
    return (
        <div className="row" >
            <div className="col-md-6">
                <img src={product.foto} alt='foto de producto' className='w-25' /><br />
                <label>{product.name}</label><br />
                <label>{product.categoria}</label><br />
                <label>{product.price}</label>
            </div>
            <div className="col-md-6">
                <ItemCount initial={1} stock={product.stock} onAdd={onAdd} />
            </div>

        </div>
    )
    
}

export default ItemDetail